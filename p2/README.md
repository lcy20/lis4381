> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>

# LIS4381 - Mobile Web Application Development

## Lucy Yates

### Project 2 Requirements:

*Sub-Heading:*

1. Develop successful delete and edit data functions
2. Successfully validate, edit, and delete data off of local lis4381 web app
3. Create RSS feed

#### README.md file should include the following items:

* Screenshot of before and after successful validations
* Screenshot of failed validation
* Screenshot of delete prompt and successfully deleted record
* Screenshot of RSS feed
* Link to local lis4381 web app


> 
>

#### Assignment Screenshots:


Carousel Page                                |                  index.php               |             editpetstore.ph (Failed Validation)
:-------------------------------------------:|:----------------------------------------:|:-------------------------------------------:
![Screenshot of carousel](img/carousel.png)  |  ![Screenshot of index](img/index.png)  |  ![Screenshot of editpetstore](img/editpetstore.png)


Failed Validation                |         Passed Validation              |     Delete Record Prompt        |   Successfully Deleted Prompt
:-------------------------------:|:--------------------------------------:|:-------------------------------:|:-------------------------------:
![](img/failedvalidation.png)    |         ![](img/passedval.png)         |      ![](img/deleterec.png)     |    ![](img/successdelete.png)


*Screenshot of RSS Feed*:

![RSS Feed](img/rssfeed.png)

*Links to Homepage*:

[My Local Host](http://localhost:8080/lis4381/index.php)


#### Links:

*Bitbucket Repo Access:*
[Bitbucket Repo Access](https://bitbucket.org/lcy20/lis4381/src/master/ "Bitbucket lis4381 Access")
