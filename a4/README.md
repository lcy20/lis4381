> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>

# LIS4381 - Mobile Web Application Development

## Lucy Yates

### Assignment 4 Requirements:

*Sub-Heading:*

1. Develop your local lis4381 web app
2. Create a favicon
3. Skill Set 10
4. Skill Set 11
5. Skill Set 12

#### README.md file should include the following items:

* Screenshot of home page
* Screenshot of failed user input validation
* Screenshot of correct user input validation
* Link to local lis4381 web app


> 
>

#### Assignment Screenshots:


Home Page                               |                  Failed Validation               |             Correct Validation
:-------------------------------------------:|:----------------------------------------:|:-------------------------------------------:
![Screenshot of First Page](img/homepage.png)  |  ![Screenshot of Second Page](img/failed.png)  |  ![Screenshot of Third Page](img/success.png)


*Screenshot of Skillsets*:

Q10 Skillset        |      Q11 Skillset    |      Q12 Skillset
:-----------------:|:-------------------:|:-------------------:
![Q10](img/skillset10.png)  |  ![Q11](img/skillset11.png)  |  ![Q12](img/skillset12.png)



*Links to Homepage*:

[My Local Host](http://localhost:8080/lis4381/index.php)


#### Links:

*Bitbucket Repo Access:*
[Bitbucket Repo Access](https://bitbucket.org/lcy20/lis4381/src/master/ "Bitbucket lis4381 Access")
