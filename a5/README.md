> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>

# LIS4381 - Mobile Web Application Development

## Lucy Yates

### Assignment 5 Requirements:

*Sub-Heading:*

1. Develop server-side validation on local lis4381 web app
2. Successfully gather and validate mySQL data on web app
3. Skill Set 13
4. Skill Set 14
5. Skill Set 15

#### README.md file should include the following items:

* Screenshot of successful and failed validations
* Screenshot of data in tables
* Link to local lis4381 web app


> 
>

#### Assignment Screenshots:


Index Page                               |                  add_petstore.php (invalid)               |             add_petstore_process.ph (Failed Validation)
:-------------------------------------------:|:----------------------------------------:|:-------------------------------------------:
![Screenshot of index.php](img/index.png)  |  ![Screenshot of Invalid](img/add_petstore_invalid.png)  |  ![Screenshot of Failed](img/add_petstore_process_failed.png)


Petstore Valid                   |  Petstore Process Valid
:-------------------------------:|:--------------------------------------:
![](img/add_petstore_valid.png)  |  ![](img/add_petstore_process_valid.png)


*Screenshot of Skillsets*:

*Screenshot of skillset 13*:

![Skillset 13](img/skillset13.png)

Q14 Skillset pt 1              |      Q14 Skillset  pt 2         |      Q14 Skillset pt 3         |      Q14 Skillset pt 4
:-----------------------------:|:-------------------------------:|:------------------------------:|:--------------------------------:
![Q14.1](img/simplecalc1.png)  |  ![Q14.2](img/simplecalc2.png)  |  ![Q14.3](img/simplecalc3.png) |    ![Q14.4](img/simplecalc4.png)

Q15 Skillset pt 1           |      Q15 Skillset  pt 2  
:--------------------------:|:---------------------------:
![Q15](img/indexwrite.png)  |  ![Q15.2](img/process.png)  


*Links to Homepage*:

[My Local Host](http://localhost:8080/lis4381/index.php)


#### Links:

*Bitbucket Repo Access:*
[Bitbucket Repo Access](https://bitbucket.org/lcy20/lis4381/src/master/ "Bitbucket lis4381 Access")
