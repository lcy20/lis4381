
public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Program loops through array of strings.");
        System.out.println("Use the following values: dog, cat, bird, fish, insect.");
        System.out.println("Use the following loop structures: for, enhanced for, while, do...while.");
        System.out.println();
        System.out.println("Note: Pretest loops: for , enhanced for , while. Posttest loop: do...while.");
        System.out.println();
    }
    public static void arrayLoops()
    {
        String[] list = {"dog", "cat", "bird", "fish", "insect"};

      // for loop
      int size = list.length;
      System.out.println("for loop:");
      for (int i = 0; i < size; i++)
      {
        System.out.println(list[i]);
      }
      System.out.println(" ");

      // enhanced for loop
      System.out.println("Enhanced for loop:");
      for (String l: list)
      {
        System.out.println(l);
      }
      System.out.println(" ");

      // while loop
      System.out.println("while loop:");
      int count = 0;
      while (count < size)
      {
        System.out.println(list[count]);
        count++;
      }
      System.out.println(" ");

      // do...while loop
      System.out.println("do...while loop:");
      int count2 = 0;
      do
      {
        System.out.println(list[count2]);
        count2++;
      } while (count2 < size);
      System.out.println(" ");
   }

}