> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4381 - Mobile Web Application Development

## Lucy Yates

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Insall Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials 
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create recipe application in Android Studio
    - Complete Skillset 1: Even or Odd
    - Complete Skillset 2: Largest Number
    - Complete Skillset 3: Arrays and Loops

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create database using MySQL Workbench
    - Create a mobile app for concert tickets
    - Complete Skillset 4, 5, and 6

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Develop your local lis4381 web app
    - Create a favicon
    - Complete Skillset 10, 11, and 12

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Develop server-side validation on local lis4381 web app
    - Successfully gather and validate mySQL data on web app
    - Complete Skillset 13, 14, and 15

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Backward-Engineer an application in Android Studio using screenshots
    - Create launcher icon image and change activity settings
    - Complete skillsets 7, 8, and 9

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Develop successful delete and edit data functions
    - Successfully validate, edit, and delete data off of local lis4381 web app
    - Create RSS feed