> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>

# LIS4381 - Mobile Web Application Development

## Lucy Yates

### Assignment 3 Requirements:

*Sub-Heading:*

1. Develop an application in Android Studio
    * Create a launcher icon image and display it in the activity
    * Add color to activity controls
    * Add border around image and button
    * Add text shadow
2. Create ERD and database diagram from mySQL
3. Skill Set 4
4. Skill Set 5
5. Skill Set 6

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshots of running application's opening user interface
* Screenshot of running application's processiing user input
* Screenshot of 10 records for each table
* Links to files a3.mwb and a3.sql


> 
>

#### Assignment Screenshots:

First Interface             |  Running User Interface
:-------------------------:|:-------------------------:
![](img/user1.png)  |  ![](img/user2.png)


*Screenshot of ERDs*

Petstore Table                               |                  Pet table               |             Customer table
:-------------------------------------------:|:----------------------------------------:|:-------------------------------------------:
![Screenshot of ERD](img/petstoretable.png)  |  ![Screenshot of ERD](img/pettable.png)  |  ![Screenshot of ERD](img/customertable.png)


*Screenshot of Skillsets*:

Q4 Skillset        |      Q5 Skillset    |      Q6 Skillset
:-----------------:|:-------------------:|:-------------------:
![Q4](img/Q4.png)  |  ![Q5](img/Q5.png)  |  ![Q6](img/Q6.png)



*Links to MySQL files*:

[a3.mwb](docs/a3.mwb)
[a3.sql](docs/a3.sql)


#### Links:

*Bitbucket Repo Access:*
[Bitbucket Repo Access](https://bitbucket.org/lcy20/lis4381/src/master/ "Bitbucket lis4381 Access")
