> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>

# LIS4381 - Mobile Web Application Development

## Lucy Yates

### Project 1 Requirements:

*Sub-Heading:*

1. Develop an application in Android Studio
3. Skill Set 7
4. Skill Set 8
5. Skill Set 9

#### README.md file should include the following items:

* Course title, name, assignment requirements
* Screenshot of running application's first user interface
* Screenshot of running application's second user interface


> 
>

#### Assignment Screenshots:

First Interface             |  Running User Interface
:-------------------------:|:-------------------------:
![](img/firstinterface.png)  |  ![](img/secondinterface.png)


*Screenshot of Skillsets*:

Q7 Skillset        |      Q8 Skillset    |      Q9 Skillset
:-----------------:|:-------------------:|:-------------------:
![Q7](img/skillset7.png)  |  ![Q8](img/skillset8.png)  |  ![Q9](img/skillset9.png)



#### Links:

*Bitbucket Repo Access:*
[Bitbucket Repo Access](https://bitbucket.org/lcy20/lis4381/src/master/ "Bitbucket lis4381 Access")
