> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Lucy Yates

### Assignment 2 Requirements:

*Sub-Heading:*

1. Develop an application in Android Studio
    * Include instructions for a recipe.
    * Include a picture of the recipe.
    * Create a button.
    * Change text color.
    * Change background color.
2. Skill Set 1: Even or Odd
3. Skill Set 2: Largest Number
4. Skill Set 3: Arrays and Loops

#### README.md file should include the following items:

* Screenshot of the buschetta recipe application running
* Screenshots of all 3 Skill Sets
* Bitbucket repo link to this assignment


> 
>

#### Assignment Screenshots:

*Screenshot of first user interface running*:

![First User Interface](img/brusch1.png)

*Screenshot of second user interface*:

![Second User Interface](img/brusch2.png)

*Screenshot of Even Or Odd Skillset*:

![Q1_EvenOrOdd](img/Q1_EvenOrOdd.png)

*Screenshot of Largest Number Skillset*:

![Q2_LargestNumber](img/Q2_LargestNumber.png)

*Screenshot of Arrays and Loops Skillset*:

![Q3_Arrays_And_Loops](img/Q3_Arrays_And_Loops.png)


#### Links:

*Bitbucket Repo Access:*
[Bitbucket Repo Access](https://bitbucket.org/lcy20/lis4381/src/master/ "Bitbucket lis4381 Access")
